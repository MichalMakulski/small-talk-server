const adminMsg = (text) => ({
  from: 'Admin',
  emoji: '🤖',
  text
});

module.exports = {adminMsg};