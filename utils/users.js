const {getRandomInt} = require('./utils');

function usersHandler() {
  let users = [];
  const emojis = ["🍏", "🍎", "🍐", "🍊", "🍋", "🍌", "🍉", "🍇", "🍓", "🍈", "🍒", "🍑", "🍍", "🥥", "🥝", "🍅", "🍆", "🥑", "🥦", "🥒", "🌶", "🌽", "🥕", "🥔", "🍠", "🥐", "🍞", "🥖", "🥨", "🧀", "🥚", "🍳", "🥞", "🥓", "🥩", "🍗", "🍖", "🌭", "🍔", "🍟", "🍕", "🥪", "🥙", "🌮", "🌯", "🥗", "🥘", "🥫", "🍝", "🍜", "🍲", "🍛", "🍣", "🍱", "🥟", "🍤", "🍙", "🍚", "🍘", "🍥", "🥠", "🍢", "🍡", "🍧", "🍨", "🍦", "🥧", "🍰", "🎂", "🍮", "🍭", "🍬", "🍫", "🍿", "🍩", "🍪", "🌰", "🥜", "🍯", "🥛", "🍼", "☕️", "🍵", "🥤", "🍶", "🍺", "🍻", "🥂", "🍷", "🥃", "🍸", "🍹", "🍾", "🥄", "🍴", "🍽", "🥣", "🥡", "🥢"];

  const addUser = (id, name) => {
    var user = {
      emoji: emojis[getRandomInt(0, emojis.length)],
      id,
      name
    };
    users.push(user);

    return user;
  };
  const removeUser = (id) => {
    var removedUser = getUser(id);

    if (removedUser) {
      users = users.filter((user) => user.id !== id);
    }

    return removedUser;
  };
  const getUser = (id) => users.filter((user) => user.id === id)[0];
  const listUsers = () => (
    users.length ?
    users.map(({emoji, name}) => `${emoji} ${name}`).join(', ') :
    'You\'re the first one!'
  );

  return {addUser, removeUser, getUser, listUsers}
}

module.exports = usersHandler();