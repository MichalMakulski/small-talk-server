const http = require('http');
const socketIO = require('socket.io');

const server = http.createServer();
const io = socketIO(server);

const port = process.env.PORT || 3000;

const users = require('./utils/users');
const {adminMsg} = require('./utils/messages');

io.on('connection', (socket) => {
  socket.on('new-user', (name, fn) => {
    if (!name || (name.toLowerCase() === 'admin')) {
      return fn({exitCode: 9});
    }

    socket.emit('newMessage', adminMsg('Welcome to SmallTalk'));
    socket.emit('newMessage', adminMsg(`Who's here? ${users.listUsers()}\n~~~~~~~~~~~`));
    socket.broadcast.emit('newMessage', adminMsg(`${name} has joined.`));

    users.addUser(socket.id, name);

    socket.on('createMessage', (message) => {
      const user = users.getUser(socket.id);

      socket.broadcast.emit('newMessage', {
        ...message,
        emoji: user.emoji,
        createdAt: new Date().getTime()
      })
    });

    socket.on('disconnect', () => {
      const user = users.removeUser(socket.id);

      socket.broadcast.emit('newMessage', adminMsg(`${name} has left.`));
    });
  });
});


server.listen(port, () => {
  console.log('App running on port', port);
});